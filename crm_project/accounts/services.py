from django.forms import ClearableFileInput


class CustomClearableFileInput(ClearableFileInput):
    """A custom image uploading widget."""
    template_name = 'accounts/widgets/clearable_file_input.html'

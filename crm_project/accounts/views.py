from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, LogoutView, PasswordChangeView
from django.urls import reverse, reverse_lazy
from django.views.generic import UpdateView, FormView, ListView

from customers.models import ContactPerson, Customer, Interaction, Project
from customers.services import GetPaginationStringMixin
from .forms import AccountFormSet, KeywordFormSet
from .models import Keyword
from .services import CustomClearableFileInput


class UserLoginView(LoginView):
    """Controller for the user authentication."""
    template_name = 'accounts/login.html'
    redirect_authenticated_user = True


class UserLogoutView(LogoutView):
    """Controller for logging out."""
    next_page = '/user/login/'


class ProfileUpdateView(LoginRequiredMixin, UpdateView):
    """Controller for the user profile update form."""
    login_url = reverse_lazy('login')
    model = User
    fields = ['first_name', 'last_name', 'email']
    template_name = 'accounts/user_profile_form.html'
    widgets = {'photo': CustomClearableFileInput}

    def get_object(self, queryset=None):
        """Return a user instance for editing."""
        return self.request.user

    def get_context_data(self, **kwargs):
        """Insert a title and a formset into the context dict."""
        context = super(ProfileUpdateView, self).get_context_data(**kwargs)
        if self.request.POST:
            context['account_formset'] = AccountFormSet(self.request.POST, self.request.FILES, instance=self.object)
            context['account_formset'].full_clean()
        else:
            context['account_formset'] = AccountFormSet(instance=self.object)
        context['title_'] = 'Мои данные'
        return context

    def form_valid(self, form):
        """Specify a user for the form and save it."""
        context = self.get_context_data(form=form)
        formset = context['account_formset']
        if formset.is_valid():
            response = super().form_valid(form)
            formset.instance = self.object
            formset.save()
            return response
        else:
            return super().form_invalid(form)

    def get_success_url(self):
        """Return redirect url."""
        return reverse('edit_profile')


class PassChangeView(LoginRequiredMixin, PasswordChangeView):
    """Controller for changing a user password."""
    template_name = 'accounts/password_change_form.html'

    def get_success_url(self):
        """Return redirect url."""
        messages.success(self.request, 'Пароль успешно изменён.')
        return reverse_lazy('edit_profile')

    def get_context_data(self, *, object_list=None, **kwargs):
        """Insert a title into the context dict."""
        context = super(PassChangeView, self).get_context_data(**kwargs)
        context['title_'] = 'Изменить пароль'
        return context


class UserCustomerListView(PermissionRequiredMixin, GetPaginationStringMixin, ListView):
    """Controller for listing all customers created by a user."""
    paginate_by = 20
    template_name = 'accounts/user_customer_list.html'
    permission_required = 'customers.add_customer'
    login_url = reverse_lazy('login')

    def get_context_data(self, *, object_list=None, **kwargs):
        """Insert a title into the context dict."""
        context = super(UserCustomerListView, self).get_context_data(**kwargs)
        context['title_'] = 'Мои заказчики'
        return context

    def get_queryset(self):
        """Return a user customer list."""
        user = self.request.user.account
        return Customer.objects.filter(manager=user)


class UserProjectListView(PermissionRequiredMixin, GetPaginationStringMixin, ListView):
    """Controller for listing all projects created by a user."""
    paginate_by = 20
    template_name = 'accounts/user_project_list.html'
    permission_required = 'customers.add_project'
    login_url = reverse_lazy('login')

    def get_queryset(self):
        """Return a user project list."""
        user = self.request.user.account
        return Project.objects.select_related('customer').filter(manager=user)

    def get_context_data(self, *, object_list=None, **kwargs):
        """Insert a title into the context dict."""
        context = super(UserProjectListView, self).get_context_data(**kwargs)
        context['title_'] = 'Мои проекты'
        return context


class UserContactPersonListView(PermissionRequiredMixin, GetPaginationStringMixin, ListView):
    """Controller for listing all contact persons created by a user."""
    paginate_by = 20
    template_name = 'accounts/user_contact_person_list.html'
    permission_required = 'customers.add_contactperson'
    login_url = reverse_lazy('login')

    def get_queryset(self):
        """Return a user contact person list."""
        user = self.request.user.account
        return ContactPerson.objects.select_related('customer').prefetch_related('contact_set').filter(manager=user)

    def get_context_data(self, *, object_list=None, **kwargs):
        """Insert a title into the context dict."""
        context = super(UserContactPersonListView, self).get_context_data(**kwargs)
        context['title_'] = 'Мои контакты'
        return context


class UserInteractionListView(PermissionRequiredMixin, GetPaginationStringMixin, ListView):
    """Controller for listing all interactions created by a user."""
    paginate_by = 10
    template_name = 'accounts/user_interaction_list.html'
    permission_required = 'customers.add_interaction'
    login_url = reverse_lazy('login')

    def get_queryset(self):
        """Return a queryset with a list of all interactions with the customer (possibly filtered) created by a user."""
        user = self.request.user.account
        user_interaction_list = Interaction.objects.filter(manager=user)
        if self.request.GET.getlist('keyword'):
            interaction_list = user_interaction_list.none()
            for keyword in self.request.GET.getlist('keyword'):
                new_queryset = user_interaction_list.select_related(
                    'contact', 'contact__contact_person', 'project', 'project__customer', 'manager', 'customer'
                ).filter(description__contains=keyword).order_by()
                interaction_list = interaction_list.union(new_queryset)
        else:
            interaction_list = user_interaction_list.select_related(
                    'contact', 'contact__contact_person', 'project', 'project__customer', 'manager', 'customer'
                ).all()
        return interaction_list.order_by('-date', '-pk')

    def get_context_data(self, *, object_list=None, **kwargs):
        """Insert a title and a search keywords into the context dict."""
        context = super(UserInteractionListView, self).get_context_data(**kwargs)
        keyword_list = Keyword.objects.filter(manager=self.request.user.account)
        context['keyword_list'] = keyword_list
        context['title_'] = 'Мои взаимодействия'
        return context


class KeywordsView(PermissionRequiredMixin, FormView):
    """Controller for editing search keyword list."""
    template_name = 'accounts/keyword_form.html'
    permission_required = 'accounts.change_keyword'
    login_url = reverse_lazy('login')


    def get_context_data(self, **kwargs):
        """Insert a title into the context dict."""
        context = super(KeywordsView, self).get_context_data(**kwargs)
        context['title_'] = 'Ключевые слова'
        return context

    def get_form(self, form_class=None):
        """Return an instance of the form to be used in this view."""
        if self.request.POST:
            form = KeywordFormSet(self.request.POST)
        else:
            form = KeywordFormSet(queryset=Keyword.objects.filter(manager=self.request.user.account))
        return form

    def form_valid(self, form):
        """Specify a user for each keyword in a formset and save it."""
        keywords = form.save(commit=False)
        for keyword in keywords:
            keyword.manager = self.request.user.account
            keyword.save()
        for keyword in form.deleted_objects:
            keyword.delete()
        return super().form_valid(form)

    def get_success_url(self):
        """Return redirect url."""
        return self.request.get_full_path()

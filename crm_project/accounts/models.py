from django.conf import settings
from django.db import models
from django.urls import reverse


class Account(models.Model):
    """A model for storing extended user accounts."""
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    """: Generic user of a Django."""
    photo = models.ImageField(verbose_name='Фотография', upload_to='photo/', default='photo/no-avatar.png')
    """: A user's photo."""

    def __str__(self):
        """Return the string representation of the entry."""
        return str(self.user.username)

    def get_absolute_url(self):
        """Return the string representation of the entry."""
        return reverse('edit_profile')

    class Meta:
        verbose_name = 'Аккаунт'
        verbose_name_plural = 'Аккаунты'


class Keyword(models.Model):
    """A model for storing user's search keywords."""
    manager = models.ForeignKey(Account, verbose_name='Менеджер', on_delete=models.CASCADE)
    """: A keyword owner."""
    word = models.CharField(max_length=20)
    """: A keyword."""

    def __str__(self):
        """Return the string representation of the entry."""
        return self.word

    class Meta:
        ordering = ['word']
        verbose_name = 'Ключевое слово'
        verbose_name_plural = 'Ключевые слова'
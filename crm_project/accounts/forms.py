from django.contrib.auth.models import User
from django.forms import inlineformset_factory, modelformset_factory

from .models import Account, Keyword
from .services import CustomClearableFileInput

AccountFormSet = inlineformset_factory(
    User, Account, fields=['photo'], can_delete=False, widgets={'photo': CustomClearableFileInput}
)
""": A formset for editing user profile."""

KeywordFormSet = modelformset_factory(Keyword, fields=['word'], labels={'word': 'Слово'}, extra=6, can_delete=True)
""": A keyword management formset."""

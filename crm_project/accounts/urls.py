from django.contrib.auth.views import PasswordChangeView
from django.urls import path

from .views import (
    PassChangeView, ProfileUpdateView,
    UserContactPersonListView, UserCustomerListView,
    UserInteractionListView, UserLoginView,
    UserLogoutView, UserProjectListView,
    KeywordsView
)

urlpatterns = [
    path('login/', UserLoginView.as_view(), name='login'),
    path('logout/', UserLogoutView.as_view(), name='logout'),
    # path('<int:pk>/private-page/', UserPrivatePageView.as_view(), name='private_page'),
    path('edit-profile/', ProfileUpdateView.as_view(), name='edit_profile'),
    path('change-password/', PassChangeView.as_view(), name='change_password'),
    path('my-customers/', UserCustomerListView.as_view(), name='my_customers'),
    path('my-projects/', UserProjectListView.as_view(), name='my_projects'),
    path('my-contacts/', UserContactPersonListView.as_view(), name='my_contacts'),
    path('my-interactions/', UserInteractionListView.as_view(), name='my_interactions'),
    path('my-keywords/', KeywordsView.as_view(), name='my_keywords'),
]

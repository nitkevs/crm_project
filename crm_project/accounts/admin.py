from django.contrib import admin

from .models import Account, Keyword

admin.site.register(Account)
admin.site.register(Keyword)

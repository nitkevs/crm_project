from django import forms
from django.forms import inlineformset_factory

from .models import ContactPerson, Contact, Interaction, Project


class DateInputWidget(forms.DateInput):
    input_type = 'date'

    def format_value(self, value):
        return value


class ProjectModelForm(forms.ModelForm):
    """A projects management form."""
    class Meta:
        model = Project
        fields = ['name', 'start_date', 'end_date', 'description', 'coast']
        widgets = {'start_date': DateInputWidget, 'end_date': DateInputWidget}

    def clean_end_date(self):
        """Validation of a project start end end dates.

        Raises:
            ValidationError: If the start date is later than the end date.
        """
        if self.cleaned_data['start_date'] > self.cleaned_data['end_date']:
            raise forms.ValidationError('Дата окончания проекта не может быть ранее даты начала работ.')
        return self.cleaned_data['end_date']


class InteractionForm(forms.ModelForm):
    """An interaction creation form."""
    class Meta:
        model = Interaction
        fields = ['date', 'project', 'contact', 'initiator', 'description', 'grade']
        widgets = {'date': DateInputWidget}


class ContactForm(forms.ModelForm):
    """A contact editing form."""
    class Meta:
        model = Contact
        fields = ['type', 'value', 'contact_person']


ContactFormset = inlineformset_factory(ContactPerson, Contact, fields=['type', 'value'], can_delete=True)
""": A contact creation and edit form."""

from django.contrib import admin

from .models import Customer, Project, ContactPerson, Contact, Interaction

admin.site.register(Customer)
admin.site.register(Project)
admin.site.register(ContactPerson)
admin.site.register(Contact)
admin.site.register(Interaction)

from django.urls import path

from .views import (
    CustomerListView, CustomerDetailView, CustomerCreateView, CustomerUpdateView, CustomerDeleteView,
    ProjectListView, ProjectDetailView, ProjectCreateView, ProjectUpdateView, ProjectDeleteView,
    ContactPersonCreateView, ContactPersonDetailView, ContactPersonUpdateView, ContactPersonDeleteView,
    ContactCreateView, ContactUpdateView, ContactDeleteView,
    InteractionListView, InteractionDetailView, InteractionCreateView, InteractionUpdateView, InteractionDeleteView,
)

urlpatterns = [
    path('', CustomerListView.as_view(), name='customer_list'),
    path('customer/<int:pk>/', CustomerDetailView.as_view(), name='customer_detail'),
    path('customer/create/', CustomerCreateView.as_view(), name='customer_create'),
    path('customer/<int:pk>/update/', CustomerUpdateView.as_view(), name='customer_update'),
    path('customer/<int:pk>/delete/', CustomerDeleteView.as_view(), name='customer_delete'),
    path('customer/<int:pk>/create-project/', ProjectCreateView.as_view(), name='project_create'),
    path('customer/<int:pk>/project-list/', ProjectListView.as_view(), name='project_list'),
    path('customer/<int:pk>/contact-person-create/', ContactPersonCreateView.as_view(), name='contact_person_create'),
    path('customer/<int:pk>/contact-create/', ContactCreateView.as_view(), name='contact_create'),
    path('customer/<int:pk>/interaction-list', InteractionListView.as_view(), name='customer_interaction_list'),
    path('customer/<int:pk>/interaction-create/', InteractionCreateView.as_view(), name='interaction_create'),
    path('project/<int:pk>/', ProjectDetailView.as_view(), name='project_detail'),
    path('project/<int:pk>/update/', ProjectUpdateView.as_view(), name='project_update'),
    path('project/<int:pk>/delete/', ProjectDeleteView.as_view(), name='project_delete'),
    path('contact_person/<int:pk>/', ContactPersonDetailView.as_view(), name='contact_person_detail'),
    path('contact_person/<int:pk>/update/', ContactPersonUpdateView.as_view(), name='contact_person_update'),
    path('contact_person/<int:pk>/delete/', ContactPersonDeleteView.as_view(), name='contact_person_delete'),
    path('contact/<int:pk>/update/', ContactUpdateView.as_view(), name='contact_update'),
    path('contact/<int:pk>/delete/', ContactDeleteView.as_view(), name='contact_delete'),
    path('interaction/<int:pk>/', InteractionDetailView.as_view(), name='interaction_detail'),
    path('interaction/<int:pk>/update/', InteractionUpdateView.as_view(), name='interaction_update'),
    path('interaction/<int:pk>/delete/', InteractionDeleteView.as_view(), name='interaction_delete'),
]
from ckeditor.fields import RichTextField
import datetime
import re

from django.core.exceptions import ValidationError
from django.db import models
from django.urls import reverse

from accounts.models import Account


class Customer(models.Model):
    """A model for storing a list of customers."""
    company_name = models.CharField(verbose_name='Название', max_length=200)
    """: The name of a customer's company."""
    address = models.CharField(verbose_name='Адрес', max_length=200, null=True, blank=True)
    """: The address of a customer's company."""
    record_created = models.DateField(verbose_name='Дата создания записи', auto_now_add=True)
    """: Date the record was created."""
    record_last_modified = models.DateField(verbose_name='Дата последнего изменения', auto_now=True)
    """: Date the record was last modified."""
    description = RichTextField(verbose_name='Описание', null=True, blank=True)
    """: Customer description."""
    manager = models.ForeignKey(Account, verbose_name='Менеджер', on_delete=models.SET_NULL, null=True)
    """: The manager who created the entry."""

    def __str__(self):
        """Return the string representation of the entry."""
        return self.company_name

    def get_absolute_url(self):
        """Return an url of the record's page."""
        return reverse('customer_detail', kwargs={'pk': self.pk})

    class Meta:
        ordering = ['-pk']
        verbose_name = 'Заказчик'
        verbose_name_plural = 'Заказчики'


class Project(models.Model):
    """A model for storing a list of projects."""
    name = models.CharField(verbose_name='Название', max_length=200)
    """: A project name."""
    description = models.TextField(verbose_name='Описание', null=True, blank=True)
    """: A project description."""
    start_date = models.DateField(verbose_name='Дата начала работы')
    """: A project start date."""
    end_date = models.DateField(verbose_name='Дата завершения')
    """: A project end date."""
    coast = models.DecimalField(verbose_name='Стоимость', max_digits=9, decimal_places=2)
    """: A project's coast."""
    customer = models.ForeignKey(Customer, verbose_name='Заказчик', on_delete=models.SET_NULL, null=True)
    """: A project customer."""
    manager = models.ForeignKey(Account, verbose_name='Менеджер', on_delete=models.SET_NULL, null=True)
    """: A manager who created the entry."""


    def __str__(self):
        """Return the string representation of the entry."""
        return f'{self.name} ({self.customer})'

    def get_absolute_url(self):
        """Return an url of the record's page."""
        return reverse('project_detail', kwargs={'pk': self.pk})

    class Meta:
        ordering = ['end_date', 'name', 'customer']
        verbose_name = 'Проект'
        verbose_name_plural = 'Проекты'


class ContactPerson(models.Model):
    """A model for storing a list of contact persons."""
    full_name = models.CharField(verbose_name='Ф.И.О.', max_length=100)
    """: A contact person full name."""
    post = models.CharField(verbose_name='Должность', max_length=50)
    """: A contact person post."""
    customer = models.ForeignKey(Customer, verbose_name='Компания', on_delete=models.CASCADE)
    """: A contact person customer."""
    manager = models.ForeignKey(Account, verbose_name='Менеджер', on_delete=models.SET_NULL, null=True)
    """: A manager who created the entry."""

    def __str__(self):
        """Return the string representation of the entry."""
        return self.full_name

    class Meta:
        ordering = ['post', 'full_name']
        verbose_name = 'Контактное лицо'
        verbose_name_plural = 'контактные лица'

    def get_absolute_url(self):
        """Return an url of the record's page."""
        return reverse('contact_person_detail', kwargs={'pk': self.pk})


class Contact(models.Model):
    """A model for storing a list of contacts."""
    TYPES = (
        (None, 'Не выбрано'),
        ('e', 'E-mail'),
        ('p', 'Телефон')
    )
    type = models.CharField(verbose_name='Способ связи', max_length=1, choices=TYPES)
    """: A contact type (phone or e-mail)."""
    value = models.CharField(verbose_name='Номер или адрес', max_length=254)
    """: A contact value(phone number or e-mail adress)."""
    company = models.ForeignKey(Customer, verbose_name='Компания', on_delete=models.CASCADE)
    """: A company that owns the contact."""
    contact_person = models.ForeignKey(ContactPerson, verbose_name='Контактное лицо', on_delete=models.CASCADE,
                                       null=True, blank=True)
    """: A contact person that owns the contact."""

    def __str__(self):
        """Return the string representation of the entry."""
        return f'{self.value} ({self.contact_person or self.company})'

    def clean_fields(self, exclude=None):
        """Validate the fields of the contact depending on its type.

        Raises:
            ValidationError: If the value of the field does not resemble the value of the specified type.
        """
        super().clean_fields(exclude=exclude)
        email_pattern = r"^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}" \
                        r"[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
        phone_pattern = r"^[+]?[\d \-()]*$"
        if self.type == 'e' and not re.match(email_pattern, self.value ):
            raise ValidationError('Это не похоже на e-mail.')
        elif self.type == 'p' and not re.match(phone_pattern, self.value):
            raise ValidationError(
                'Это не похоже на номер телефона. '
                'Введите что-то типа "+380(012)345-67-89" либо номер в другом разумном формате.'
            )

    class Meta:
        ordering = ['contact_person', 'type']
        verbose_name = 'Контакт'
        verbose_name_plural = 'Контакты'


class Interaction(models.Model):
    """A model for storing a list of interactions with a customers."""
    date = models.DateField(verbose_name='Дата', default=datetime.date.today)
    """: An interaction date."""
    customer = models.ForeignKey(Customer, verbose_name='Заказчик', on_delete=models.SET_NULL, null=True)
    """: A customer."""
    project = models.ForeignKey(Project, verbose_name='Проект', on_delete=models.SET_NULL, null=True)
    """: An interaction project."""
    contact = models.ForeignKey(Contact, verbose_name='Канал связи', on_delete=models.SET_NULL, null=True)
    """: An interaction contact."""
    manager = models.ForeignKey(Account, verbose_name='Менеджер', on_delete=models.SET_NULL, null=True)
    """: A manager who created the entry."""
    initiator = models.BooleanField(verbose_name='Инициатива заказчика', default=True)
    """: An interaction initiator."""
    description = models.TextField(verbose_name='Описание', null=True, blank=True)
    """: An interaction description."""
    GRADES = (
        (-5, -5), (-4, -4), (-3, -3), (-2, -2), (-1, -1),
        (0, 0), (1, 1), (2, 2), (3, 3), (4, 4), (5, 5),
    )
    grade = models.SmallIntegerField(verbose_name='Оценка', choices=GRADES)
    """: An interaction grade."""

    def __str__(self):
        """Return the string representation of the entry."""
        return f'{self.project} - {self.date}'

    def get_absolute_url(self):
        """Return an url of the record's page."""
        return reverse('interaction_detail', kwargs={'pk': self.pk})

    def clean_fields(self, exclude=None):
        """Validate the date of interaction.

        Raises:
            ValidationError: If the date of interaction is later than today.
        """
        super().clean_fields(exclude=exclude)
        if self.date > datetime.date.today():
            raise ValidationError({
                'date': 'Дата взаимодействия не может быть позже сегодняшней даты.'
            })

    class Meta:
        ordering = ['-date', '-pk']
        verbose_name = 'Взаимодействие'
        verbose_name_plural = 'Взаимодействия'


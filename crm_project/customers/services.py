class GetPaginationStringMixin:
    """Mixin that creates a string of links for pagination."""
    def get_context_data(self, *, object_list=None, **kwargs):
        """Insert the a string of links for pagination into the context dict."""
        context = super().get_context_data(**kwargs)
        self.page_obj = context['page_obj']
        self.page_obj.paginator.pages_list = list(range(1, self.page_obj.paginator.num_pages + 1))
        paginator_items_list = []
        current_page_number = self.page_obj.number
        last_number = 1
        for number in self.page_obj.paginator.pages_list:
            if 6 >= current_page_number > number:
                paginator_items_list.append(number)
                last_number = number
            elif (number <= 2) or (current_page_number - 2 <= number < current_page_number):
                paginator_items_list.append(number)
                last_number = number
            elif number == current_page_number:
                paginator_items_list.append(number)
                last_number = number
            elif (self.page_obj.paginator.num_pages - current_page_number < 6) and (number > current_page_number):
                paginator_items_list.append(number)
                last_number = number
            elif (current_page_number + 2 >= number > current_page_number) or (number > self.page_obj.paginator.num_pages - 2):
                paginator_items_list.append(number)
                last_number = number
            elif last_number != self.page_obj.paginator.ELLIPSIS:
                paginator_items_list.append(self.page_obj.paginator.ELLIPSIS)
                last_number = self.page_obj.paginator.ELLIPSIS
        context['paginator_items_list'] = paginator_items_list
        return context


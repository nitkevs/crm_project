from django.contrib.auth.mixins import LoginRequiredMixin, PermissionRequiredMixin
from django.shortcuts import get_list_or_404, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views.generic import CreateView, DetailView, DeleteView, ListView, UpdateView

from accounts.models import Keyword
from .forms import ContactFormset, ContactForm, InteractionForm, ProjectModelForm
from .models import Contact, ContactPerson, Customer, Interaction, Project
from .services import GetPaginationStringMixin


class CustomerListView(GetPaginationStringMixin, ListView):
    """Controller for listing all customers."""
    paginate_by = 20

    def get_queryset(self):
        search = self.request.GET.get('search')
        only_mine = self.request.GET.get('only_mine')
        self.ordering = self.request.GET.get('sort')
        filters = {}
        if search:
            filters['company_name__icontains'] = search
        if only_mine:
            filters['manager'] = self.request.user.account
        qeuryset = Customer.objects.filter(**filters)
        return qeuryset.order_by(self.ordering if self.ordering in {'company_name', '-company_name', 'record_created', '-record_created'} else '-pk')

    def get_context_data(self, *, object_list=None, **kwargs):
        """Insert the ordering modes into the context dict."""
        context = super(CustomerListView, self).get_context_data(**kwargs)
        context['ordering_values'] = {
            'company_name': 'А...Я',
            '-company_name': 'Я...А',
            '-record_created': 'Сначала новые',
            'record_created': 'Сначала старые',
            None: 'Без сортировки',
        }
        context['current_order'] = self.ordering
        return context


class CustomerDetailView(LoginRequiredMixin, DetailView):
    """Controller for customer detail view."""
    login_url = reverse_lazy('login')

    def get_object(self, queryset=None):
        self.customer = Customer.objects.prefetch_related(
            'interaction_set', 'project_set', 'contact_set', 'contactperson_set__contact_set', 'project_set__manager',
            'interaction_set__manager', 'interaction_set__project', 'interaction_set__contact', 'interaction_set__contact__contact_person',
        ).get(pk=self.kwargs.get('pk'))
        return self.customer

    def get_context_data(self, **kwargs):
        """Insert a page title and customer contacts, not belonging to a specific contact person into the context dict."""
        context = super().get_context_data(**kwargs)
        context['company_contacts'] = self.customer.contact_set.filter(contact_person=None)
        context['title_'] = self.customer.company_name
        return context


class CustomerCreateView(PermissionRequiredMixin, CreateView):
    """Controller for the customer creation form."""
    model = Customer
    fields = ['company_name', 'address', 'description']
    permission_required = 'customers.add_customer'
    login_url = reverse_lazy('login')

    def form_valid(self, form):
        """Specify a manager for the customer."""
        form.instance.manager = self.request.user.account
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        """Insert a page title into the context dict."""
        context = super().get_context_data(**kwargs)
        context['title_'] = 'Новый заказчик'
        return context


class CustomerUpdateView(PermissionRequiredMixin, UpdateView):
    """Controller for the customer edit form."""
    model = Customer
    fields = ['company_name', 'address', 'description']
    permission_required = 'customers.change_customer'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        """Insert a page title into the context dict."""
        context = super().get_context_data(**kwargs)
        context['title_'] = f'Изменить «{self.object.company_name}»'
        return context


class CustomerDeleteView(PermissionRequiredMixin, DeleteView):
    """Controller for deleting the customer."""
    model = Customer
    success_url = '/'
    permission_required = 'customers.delete_customer'
    login_url = reverse_lazy('login')


class ProjectListView(LoginRequiredMixin, GetPaginationStringMixin, ListView):
    """Controller for listing all projects of a specific customer."""
    paginate_by = 10
    template_name = 'customers/project_list.html'
    context_object_name = 'project_list'
    ordering = ['end_date', 'name']
    login_url = reverse_lazy('login')

    def get_queryset(self):
        """Return a queryset with a list of projects of a specific customer"""
        customer_pk = self.kwargs.get('pk')
        self.customer = Customer.objects.get(pk=customer_pk)
        queryset = get_list_or_404(Project, customer=self.customer)
        return queryset

    def get_context_data(self, *, object_list=None, **kwargs):
        """Insert a page title and current customer into the context dict."""
        context = super(ProjectListView, self).get_context_data(**kwargs)
        context['customer'] = self.customer
        context['title_'] = f'Список проектов «{self.customer}»'
        return context


class ProjectDetailView(PermissionRequiredMixin, DetailView):
    """Controller for project detail view."""
    model = Project
    permission_required = 'customers.view_project'
    login_url = reverse_lazy('login')

    def get_context_data(self, *, object_list=None, **kwargs):
        """Insert a page title, keywords and a project interaction list (possibly filtered) into the context dict."""
        context = super(ProjectDetailView, self).get_context_data(**kwargs)
        keyword_list = Keyword.objects.filter(manager=self.request.user.account)
        context['keyword_list'] = keyword_list
        project_interaction_list = Interaction.objects.filter(project=self.object)
        if self.request.GET.getlist('keyword'):
            interaction_list = project_interaction_list.none()
            for keyword in self.request.GET.getlist('keyword'):
                new_queryset = project_interaction_list.select_related(
                    'contact', 'contact__contact_person', 'project', 'project__customer', 'manager', 'customer'
                ).filter(description__contains=keyword).order_by()
                interaction_list = interaction_list.union(new_queryset)
        else:
            interaction_list = project_interaction_list.select_related(
                'contact', 'contact__contact_person', 'project', 'project__customer', 'manager', 'customer'
            )
        context['interaction_list'] = interaction_list.order_by('-date', '-pk')
        context['title_'] = f'Проект «{self.object.name}»'
        return context


class ProjectCreateView(PermissionRequiredMixin, CreateView):
    """Controller for the project creation form."""
    model = Project
    form_class = ProjectModelForm
    permission_required = 'customers.add_project'
    login_url = reverse_lazy('login')

    def form_valid(self, form):
        """Specify a manager and a customer for the project."""
        customer = Customer.objects.get(pk=self.kwargs.get('pk'))
        form.instance.customer = customer
        form.instance.manager = self.request.user.account
        return super().form_valid(form)

    def get_context_data(self, **kwargs):
        """Insert a page title into the context dict."""
        context = super().get_context_data(**kwargs)
        context['title_'] = 'Новый проект'
        return context


class ProjectUpdateView(PermissionRequiredMixin, UpdateView):
    """Controller for the project edit form."""
    model = Project
    form_class = ProjectModelForm
    permission_required = 'customers.change_project'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        """Insert a page title into the context dict."""
        context = super().get_context_data(**kwargs)
        context['title_'] = f'Изменить проект «{self.object.name}»'
        return context


class ProjectDeleteView(PermissionRequiredMixin, DeleteView):
    """Controller for deleting the project."""
    model = Project
    permission_required = 'customers.delete_project'
    login_url = reverse_lazy('login')

    def get_success_url(self):
        """Return a redirect url."""
        customer = self.object.customer
        return reverse('customer_detail', kwargs={'pk': customer.pk})


class ContactPersonDetailView(PermissionRequiredMixin, DetailView):
    """Controller for the customer's contact person detail view."""
    model = ContactPerson
    permission_required = 'customers.view_contactperson'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        """Insert a page title into the context dict."""
        context = super().get_context_data(**kwargs)
        context['title_'] = self.object
        return context


class ContactPersonCreateView(PermissionRequiredMixin, CreateView):
    """Controller for the customer's contact person creation form."""
    model = ContactPerson
    fields = ['full_name', 'post']
    permission_required = 'customers.add_contactperson'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        """Insert a page title and a formset for the contact person contacts into the context dict."""
        context = super(ContactPersonCreateView, self).get_context_data(**kwargs)
        if self.request.POST:
            context['contact_formset'] = ContactFormset(self.request.POST)
        else:
            context['contact_formset'] = ContactFormset()
            context['title_'] = f"Новое контактное лицо ({Customer.objects.get(pk=self.kwargs.get('pk'))})"
        return context

    def form_valid(self, form):
        """Specify a manager and a customer for the contact person."""
        self.customer = Customer.objects.get(pk=self.kwargs.get('pk'))
        context = self.get_context_data(form=form)
        formset = context['contact_formset']
        form.instance.customer = self.customer
        form.instance.manager = self.request.user.account
        if formset.is_valid():
            response = super().form_valid(form)
            contacts = formset.save(commit=False)
            for contact in contacts:
                contact.contact_person = self.object
                contact.company = self.customer
                contact.save()
            return response
        else:
            return super().form_invalid(form)

    def get_success_url(self):
        """Return a redirect url."""
        url = reverse('customer_detail', kwargs={'pk': self.customer.pk})
        return url


class ContactPersonUpdateView(PermissionRequiredMixin, UpdateView):
    """Controller for the contact person edit form."""
    model = ContactPerson
    fields = ['full_name', 'post']
    permission_required = 'customers.change_contactperson'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        """Insert a page title and a formset for the contact person contacts into the context dict."""
        context = super(ContactPersonUpdateView, self).get_context_data(**kwargs)
        if self.request.POST:
            context['contact_formset'] = ContactFormset(self.request.POST, instance=self.object)
        else:
            context['contact_formset'] = ContactFormset(instance=self.object)
            context['title_'] = f'Изменить {self.object}'
        return context

    def form_valid(self, form):
        """Specify a contact person and a customer for each contact in a formset."""
        self.customer = Customer.objects.get(pk=self.kwargs.get('pk'))
        context = self.get_context_data(form=form)
        formset = context['contact_formset']
        if formset.is_valid():
            response = super().form_valid(form)
            contacts = formset.save(commit=False)
            for contact in contacts:
                contact.contact_person = self.object
                contact.company = self.customer
                contact.save()
            for contact in formset.deleted_objects:
                contact.delete()
            return response
        else:
            return super().form_invalid(form)

    def get_success_url(self):
        """Return a redirect url."""
        url = reverse('contact_person_detail', kwargs={'pk': self.customer.pk})
        return url


class ContactPersonDeleteView(PermissionRequiredMixin, DeleteView):
    """Controller for deleting a contact person."""
    model = ContactPerson
    permission_required = 'customers.delete_contactperson'
    login_url = reverse_lazy('login')

    def get_success_url(self):
        """Return a redirect url."""
        self.customer = self.object.customer
        url = reverse('customer_detail', kwargs={'pk': self.customer.pk})
        return url


class ContactCreateView(PermissionRequiredMixin, CreateView):
    """Controller for the contact creation form."""
    template_name = 'customers/contact_form.html'
    permission_required = 'customers.add_contact'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        """Insert a page title and the customer company name into the context dict."""
        context = super(ContactCreateView, self).get_context_data(**kwargs)
        self.customer = Customer.objects.get(pk=self.kwargs.get('pk'))
        context['company'] = self.customer
        context['title_'] = f'Новый контакт ({self.customer})'
        return context

    def get_form(self, form_class=None):
        """Return an instance of the form to be used in this view.

        Set up a contact person list for the select form widget.
        """
        form = ContactForm(**self.get_form_kwargs())
        self.customer = Customer.objects.get(pk=self.kwargs.get('pk'))
        form.fields['contact_person'].queryset = ContactPerson.objects.filter(customer=self.customer).order_by('full_name')
        return form

    def form_valid(self, form):
        """Specify a customer for the contact."""
        self.customer = Customer.objects.get(pk=self.kwargs.get('pk'))
        form.instance.company = self.customer
        return super().form_valid(form)

    def get_success_url(self):
        """"Return a redirect url."""
        url = reverse('customer_detail', kwargs={'pk': self.customer.pk})
        return url


class ContactUpdateView(PermissionRequiredMixin, UpdateView):
    """Controller for the contact edit form."""
    model = Contact
    fields = ['type', 'value', 'contact_person']
    permission_required = 'customers.change_contact'
    login_url = reverse_lazy('login')

    def get_success_url(self):
        """Return a redirect url."""
        self.customer = self.object.company
        url = reverse('customer_detail', kwargs={'pk': self.customer.pk})
        return url

    def get_form(self, form_class=None):
        """Return an instance of the form to be used in this view.

        Set up a contact person list for the select form widget.
        """
        form = ContactForm(**self.get_form_kwargs())
        form.fields['contact_person'].queryset = ContactPerson.objects.filter(customer=self.object.company).order_by('full_name')
        return form

    def get_context_data(self, **kwargs):
        """Insert a page title into the context dict."""
        context = super().get_context_data(**kwargs)
        context['title_'] = f'Редактируем контакт ({self.object.company})'
        return context


class ContactDeleteView(PermissionRequiredMixin, DeleteView):
    """Controller for deleting the contact."""
    model = Contact
    permission_required = 'customers.delete_contact'
    login_url = reverse_lazy('login')

    def get_success_url(self):
        """Return a redirect url."""
        self.customer = self.object.company
        url = reverse('customer_detail', kwargs={'pk': self.customer.pk})
        return url


class InteractionListView(PermissionRequiredMixin, GetPaginationStringMixin, ListView):
    """Controller for listing all interactions for the customer."""
    paginate_by = 20
    permission_required = 'customers.view_interaction'
    login_url = reverse_lazy('login')

    def get_queryset(self):
        """Return a queryset with a list of all interactions with the customer (possibly filtered)."""
        customer = get_object_or_404(Customer, pk=self.kwargs.get('pk'))
        if self.request.GET.getlist('keyword'):
            interaction_list = customer.interaction_set.none()
            for keyword in self.request.GET.getlist('keyword'):
                new_queryset = customer.interaction_set.select_related(
                    'contact', 'contact__contact_person', 'project', 'project__customer', 'manager', 'customer'
                ).filter(description__contains=keyword).order_by()
                interaction_list = interaction_list.union(new_queryset)
        else:
            interaction_list = customer.interaction_set.select_related(
                'contact', 'contact__contact_person', 'project', 'project__customer', 'manager', 'customer'
            )
        return interaction_list.order_by('-date', '-pk')

    def get_context_data(self, **kwargs):
        """Insert a page title and the customer and a search keyword list into the context dict."""
        context = super(InteractionListView, self).get_context_data(**kwargs)
        customer = Customer.objects.get(pk=self.kwargs.get('pk'))
        context['customer'] = customer
        keyword_list = Keyword.objects.filter(manager=self.request.user.account)
        context['keyword_list'] = keyword_list
        context['title_'] = f'Взаимодействия с «{customer}»'
        return context


class InteractionDetailView(PermissionRequiredMixin, DetailView):
    """Controller for interaction detail view."""
    model = Interaction
    permission_required = 'customers.view_interaction'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        """Insert a page title into the context dict."""
        context = super().get_context_data(**kwargs)
        context['title_'] = f'Взаимодействие с «{self.object.customer}»'
        return context


class InteractionCreateView(PermissionRequiredMixin, CreateView):
    """Controller for the interaction creation form."""
    template_name = 'customers/interaction_form.html'
    permission_required = 'customers.add_interaction'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        """Insert a page and the customer company name into the context dict."""
        context = super(InteractionCreateView, self).get_context_data(**kwargs)
        customer = Customer.objects.get(pk=self.kwargs.get('pk'))
        context['customer_name'] = customer.company_name
        context['title_'] = f'Новое взаимодействие с «{self.customer}»'
        return context

    def form_valid(self, form):
        """Specify a manager and a customer for the interaction."""
        project = form.cleaned_data['project']
        customer = project.customer
        form.instance.customer = customer
        form.instance.manager = self.request.user.account
        return super().form_valid(form)

    def get_form(self, form_class=None):
        """Return an instance of the form to be used in this view.

        Set up a project list and a contact person list for the select form widgets.
        """
        form = InteractionForm(**self.get_form_kwargs())
        self.customer = Customer.objects.get(pk=self.kwargs.get('pk'))
        form.fields['project'].queryset = Project.objects.filter(customer=self.customer.pk).order_by('name')
        form.fields['contact'].queryset = Contact.objects.filter(company=self.customer.pk, contact_person__isnull=False)\
            .order_by('value', 'contact_person')
        return form


class InteractionUpdateView(PermissionRequiredMixin, UpdateView):
    """Controller for the interaction edit form."""
    template_name = 'customers/interaction_form.html'
    permission_required = 'customers.change_interaction'
    login_url = reverse_lazy('login')

    def get_object(self, queryset=None):
        """Return an interaction instance, if it belongs to a current authenticated user."""
        interaction = Interaction.objects.get(pk=self.kwargs.get('pk'), manager=self.request.user.account)
        return interaction

    def get_form(self, form_class=None):
        """Return an instance of the form to be used in this view.

        Set up a project list and a contact person list for the select form widgets.
        """
        form = InteractionForm(**self.get_form_kwargs())
        form.fields['project'].queryset = Project.objects.filter(customer=self.object.customer.pk).order_by('name')
        form.fields['project'].empty_label = None
        form.fields['contact'].queryset = Contact.objects.filter(company=self.object.customer.pk, contact_person__isnull=False)\
            .order_by('value', 'contact_person')
        form.fields['contact'].empty_label = None
        return form

    def get_context_data(self, **kwargs):
        """Insert a page title into the context dict."""
        context = super().get_context_data(**kwargs)
        context['title_'] = f'Изменить взаимодействие с «{self.object.customer}»'
        return context

class InteractionDeleteView(PermissionRequiredMixin, DeleteView):
    """Controller for deleting the interaction."""
    permission_required = 'customers.delete_interaction'
    login_url = reverse_lazy('login')

    def get_object(self, queryset=None):
        """Return an interaction instance, if it belongs to a current authenticated user."""
        interaction = Interaction.objects.get(pk=self.kwargs.get('pk'), manager=self.request.user.account)
        return interaction

    def get_success_url(self):
        """Return a redirect url."""
        customer = self.object.customer
        return reverse('customer_interaction_list', kwargs={'pk': customer.pk})
